#include <vtkCellData.h>
#include <vtkPolyDataMapper.h>
#include <vtkProperty.h>
#include <vtkTransform.h>
#include <vtkTransformPolyDataFilter.h>
#include <vtkPolyDataMapper.h>
#include <vtkMath.h>

#include "..\header\Shapes.h"


CShapes::CShapes()
{
	mPolyData = nullptr;
	mActor = nullptr;
}

CShapes::~CShapes()
{
}

void CShapes::InitShape()
{
	if (nullptr != mPolyData)
	{
		vtkSmartPointer<vtkPolyDataMapper> mapper =
			vtkSmartPointer<vtkPolyDataMapper>::New();
		mapper->SetInputData(mPolyData);
		mActor = vtkSmartPointer<vtkActor>::New();
		mActor->SetMapper(mapper);
		mActor->Modified();
	}
}

void CShapes::ApplyColor()
{
	if (nullptr != mActor)
	{
		mActor->GetProperty()->SetColor(mColor);
		mActor->Modified();
	}
}

void CShapes::SetType(ShapesType iType)
{
	vtkIntArray* type = vtkIntArray::New();
	type->SetNumberOfComponents(1);
	type->SetName("ShapesType");
	int castedInt = iType;
	type->InsertNextTupleValue(&castedInt);
	mPolyData->GetFieldData()->AddArray(type);
	//mPolyData->GetCellData()->GetAttribute(0)->SetComponentName(0,iTypeName);
}

vtkSmartPointer<vtkPolyData> CShapes::GetPolyData()
{
	return mPolyData;
}

vtkSmartPointer<vtkActor> CShapes::GetActor()
{
	return mActor;
}

void CShapes::Reloacte()
{
	vtkSmartPointer<vtkTransform> translation = vtkSmartPointer<vtkTransform>::New();
	translation->Translate(vtkMath::Random(-20, 20.0), vtkMath::Random(-20, 20.0), vtkMath::Random(-20.0, 20.0));
	vtkSmartPointer<vtkTransformPolyDataFilter> transformFilter = vtkSmartPointer<vtkTransformPolyDataFilter>::New();
	transformFilter->SetInputData(mPolyData);
	transformFilter->SetTransform(translation);
	transformFilter->Update();
	mPolyData = transformFilter->GetOutput();

	vtkSmartPointer<vtkPolyDataMapper> transformedMapper = vtkSmartPointer<vtkPolyDataMapper>::New();
	transformedMapper->SetInputConnection(transformFilter->GetOutputPort());
	vtkSmartPointer<vtkActor> transformedActor = vtkSmartPointer<vtkActor>::New();
	mActor->SetMapper(transformedMapper);
	mActor->Modified();
}