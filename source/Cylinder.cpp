#include <vtkTransform.h>
#include <vtkPolyDataMapper.h>
#include <vtkTransformPolyDataFilter.h>

#include "..\header\Cylinder.h"

double CCylinder::mHeight = 10;
CCylinder::CCylinder()
{
	
	InitShape();
}


CCylinder::~CCylinder()
{
}

//CCylinder::CCylinder(CCylinder& obj)
//{
//	mPolyData = vtkSmartPointer<vtkPolyData>::New();
//	mPolyData->DeepCopy(obj.mPolyData);
//	mActor = obj.mActor;
//}

void CCylinder::InitShape()
{
	vtkSmartPointer<vtkCylinderSource> cylinderSrc = vtkSmartPointer<vtkCylinderSource>::New();
	cylinderSrc->SetCenter(0, 0, 0);
	cylinderSrc->SetRadius(5);
	cylinderSrc->SetHeight(mHeight);
	cylinderSrc->SetResolution(100);
	cylinderSrc->Update();

	mPolyData = vtkSmartPointer<vtkPolyData>::New();
	mPolyData->ShallowCopy(cylinderSrc->GetOutput());
	SetType(sCylinder);
	CShapes::InitShape();
}

void CCylinder::ApplyColor()
{
	mColor[0] = 1;	mColor[1] = 0;	mColor[2] = 0;
	CShapes::ApplyColor();
}

void CCylinder::MoveInDirection(double* dir)
{
	// Set up the transform filter  
	vtkSmartPointer<vtkTransform> translation =    vtkSmartPointer<vtkTransform>::New();  
	translation->Translate(dir);
	vtkSmartPointer<vtkTransformPolyDataFilter> transformFilter =    vtkSmartPointer<vtkTransformPolyDataFilter>::New();  
	transformFilter->SetInputData(mPolyData);  
	transformFilter->SetTransform(translation);  
	transformFilter->Update();
	mPolyData = transformFilter->GetOutput();

	vtkSmartPointer<vtkPolyDataMapper> transformedMapper =	vtkSmartPointer<vtkPolyDataMapper>::New();
	transformedMapper->SetInputConnection(transformFilter->GetOutputPort());
	vtkSmartPointer<vtkActor> transformedActor = vtkSmartPointer<vtkActor>::New();
	mActor->SetMapper(transformedMapper);
	mActor->Modified();
}

void CCylinder::IncreaseHeight()
{
	vtkSmartPointer<vtkCylinderSource> cylinderSrc = vtkSmartPointer<vtkCylinderSource>::New();
	cylinderSrc->SetCenter(mActor->GetCenter());
	cylinderSrc->SetRadius(5);
	mHeight += 1;
	cylinderSrc->SetHeight(mHeight);
	cylinderSrc->SetResolution(100);
	cylinderSrc->Update();

	mPolyData = vtkSmartPointer<vtkPolyData>::New();
	mPolyData->ShallowCopy(cylinderSrc->GetOutput());
	vtkSmartPointer<vtkPolyDataMapper> transformedMapper = vtkSmartPointer<vtkPolyDataMapper>::New();
	transformedMapper->SetInputData(mPolyData);
	vtkSmartPointer<vtkActor> transformedActor = vtkSmartPointer<vtkActor>::New();
	mActor->SetMapper(transformedMapper);
	mActor->Modified();
}

void CCylinder::BringToCenter()
{
	vtkSmartPointer<vtkCylinderSource> cylinderSrc = vtkSmartPointer<vtkCylinderSource>::New();
	cylinderSrc->SetCenter(0, 0, 0);
	cylinderSrc->SetRadius(5);
	cylinderSrc->SetHeight(mHeight);
	cylinderSrc->SetResolution(100);
	cylinderSrc->Update();

	mPolyData = vtkSmartPointer<vtkPolyData>::New();
	mPolyData->ShallowCopy(cylinderSrc->GetOutput());
	vtkSmartPointer<vtkPolyDataMapper> transformedMapper = vtkSmartPointer<vtkPolyDataMapper>::New();
	transformedMapper->SetInputData(mPolyData);
	vtkSmartPointer<vtkActor> transformedActor = vtkSmartPointer<vtkActor>::New();
	mActor->SetMapper(transformedMapper);
	mActor->Modified();
}