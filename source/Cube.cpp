#include <vtkCubeSource.h>
#include <vtkMath.h>

#include "..\header\Cube.h"


CCube::CCube()
{
	InitShape();
}


CCube::~CCube()
{
}


void CCube::InitShape()
{
	vtkSmartPointer<vtkCubeSource> cubeSrc = vtkSmartPointer<vtkCubeSource>::New();
	cubeSrc->SetCenter(vtkMath::Random(-20, 20.0), vtkMath::Random(-20, 20.0), vtkMath::Random(-20.0, 20.0));
	cubeSrc->SetXLength(7);
	cubeSrc->SetYLength(7);
	cubeSrc->SetZLength(7);
	cubeSrc->Update();

	mPolyData = vtkSmartPointer<vtkPolyData>::New();
	mPolyData->ShallowCopy(cubeSrc->GetOutput());
	SetType(sCube);
	CShapes::InitShape();
}

void CCube::ApplyColor()
{
	mColor[0] = 1;	mColor[1] = 0;	mColor[2] = 0;
	CShapes::ApplyColor();
}