#include <vtkSphereSource.h>
#include <vtkMath.h>
#include <vtkProperty.h>

#include "..\header\Sphere.h"


CSphere::CSphere(ShapesType type) :mType(type)
{
	InitShape();
	ApplyColor();
}


CSphere::~CSphere()
{
}

void CSphere::InitShape()
{
	vtkSmartPointer<vtkSphereSource> aSphereSrc = vtkSmartPointer<vtkSphereSource>::New();
	aSphereSrc->SetCenter(vtkMath::Random(-20, 20.0), vtkMath::Random(-20, 20.0), vtkMath::Random(-20.0, 20.0));
	aSphereSrc->SetRadius(5);
	aSphereSrc->SetThetaResolution(100);
	aSphereSrc->SetPhiResolution(100);
	aSphereSrc->Update();

	mPolyData = vtkSmartPointer<vtkPolyData>::New();
	mPolyData->ShallowCopy(aSphereSrc->GetOutput());
	SetType(mType);
	CShapes::InitShape();
}

void CSphere::ApplyColor()
{
	if (sSphere1 == mType)
	{
		mColor[0] = 1;	mColor[1] = 0;	mColor[2] = 0;
	}
	else if (sSphere2 == mType)
	{
		mColor[0] = 0; mColor[1] = 1;	mColor[2] = 0;
	}
	else
	{
		mColor[0] = 0;	mColor[1] = 0;	mColor[2] = 1;
	}
	CShapes::ApplyColor();
}

void CSphere::DoHide()
{
	if (sSphere1 == mType)
	{
		mActor->VisibilityOff();
		mActor->Modified();
	}
}