#include <vtkAutoInit.h>

//#ifdef VTK_OPENGL2
//VTK_MODULE_INIT(vtkRenderingOpenGL2);
//VTK_MODULE_INIT(vtkRenderingVolumeOpenGL2);
//#else if
VTK_MODULE_INIT(vtkRenderingOpenGL);
//#endif

VTK_MODULE_INIT(vtkInteractionStyle);
VTK_MODULE_INIT(vtkRenderingFreeType);

#include <vtkSphereSource.h>
#include <vtkPolyData.h>
#include <vtkSmartPointer.h>
#include <vtkPolyData.h>
#include <vtkCellData.h>
#include <vtkPolyDataMapper.h>
#include <vtkActor.h>
#include <vtkRenderWindow.h>
#include <vtkRenderer.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkInteractorStyleFlight.h>
#include <vtkObjectFactory.h>

#include "../header/Shapes.h"
#include "../header/Cylinder.h"
#include "../header/Sphere.h"
#include "../header/cube.h"
#include "../header/CInteractorStyle.h"
#include "../header/EventManager.h"


int main(int, char *[])
{
	CCylinder aCylinder;
	CSphere aSphere1(sSphere1);
	CSphere aSphere2(sSphere2);
	CSphere aSphere3(sSphere3);
	CCube aCube;

	std::vector<CShapes*> shapes = { &aSphere1, &aSphere2, &aSphere3, &aCube };
	CEventManager eventManger(shapes, &aCylinder);

	vtkSmartPointer<vtkRenderer> renderer =
		vtkSmartPointer<vtkRenderer>::New();
	vtkSmartPointer<vtkRenderWindow> renderWindow =
		vtkSmartPointer<vtkRenderWindow>::New();
	renderWindow->AddRenderer(renderer);
	vtkSmartPointer<vtkRenderWindowInteractor> renderWindowInteractor =
		vtkSmartPointer<vtkRenderWindowInteractor>::New();
	renderWindowInteractor->SetRenderWindow(renderWindow);
	renderWindow->SetSize(600, 600);


	renderer->AddActor(aCylinder.GetActor());
	renderer->AddActor(aSphere1.GetActor());
	renderer->AddActor(aSphere2.GetActor());
	renderer->AddActor(aSphere3.GetActor());
	renderer->AddActor(aCube.GetActor());
	renderer->SetBackground(.3, .6, .3); // Background color green

	vtkSmartPointer<CInteractorStyle> style =
		vtkSmartPointer<CInteractorStyle>::New();
	style->SetObject(&aCylinder);
	style->SetEventManager(eventManger);
	renderWindowInteractor->SetInteractorStyle(style);
	style->SetDefaultRenderer(renderer);
	renderWindow->Render();
	renderWindowInteractor->Start();

	return EXIT_SUCCESS;
}