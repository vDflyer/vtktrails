#include <vtkFieldData.h>
#include <vtkMapper.h>

#include "..\header\Sphere.h"
#include "..\header\Cube.h"
#include "..\header\Cylinder.h"
#include "..\header\EventManager.h"

CEventManager::CEventManager()
{

}
CEventManager::CEventManager(std::vector<CShapes*> shapes, CCylinder* handler) :mShapes(shapes), mHandler(handler)
{
}


CEventManager::~CEventManager()
{
}

void CEventManager::Collided(vtkActor* aShapeActor, vtkRenderer* renderer)
{
	ShapesType collidedShape = GetShapeType(aShapeActor);
	switch (collidedShape)
	{
	case sSphere1:
		Sphere1Rection(aShapeActor, renderer);
		break;
	case sSphere2:
		Sphere2Rection();
		break;
	case sSphere3:
		break;
	case sCube:
		CubeRection();
		break;
	default:
		break;
	}
	
}

void CEventManager::Sphere1Rection(vtkActor* aShapeActor, vtkRenderer* renderer)
{
	CSphere* pSphere1 = (CSphere*)GetShape(aShapeActor);
	if (nullptr != pSphere1)
	{
		renderer->RemoveActor(pSphere1->GetActor());
	}
	CSphere* pSphere2 = (CSphere*)GetShape(ShapesType::sSphere2);
	if (nullptr != pSphere2)
	{
		pSphere2->Reloacte();
	}
	CSphere* pSphere3 = (CSphere*)GetShape(ShapesType::sSphere3);
	if (nullptr != pSphere3)
	{
		pSphere3->Reloacte();
	}
	CCube* pCube = (CCube*)GetShape(ShapesType::sCube);
	if (nullptr != pCube)
	{
		pCube->Reloacte();
	}
}

void CEventManager::Sphere2Rection()
{
	mHandler->IncreaseHeight();
}

void CEventManager::CubeRection()
{
	mHandler->BringToCenter();
}

CShapes* CEventManager::GetShape(vtkActor* aShapeActor)
{
	for (auto it : mShapes)
	{
		if (aShapeActor == it->GetActor())
		{
			return it;
		}
	}
	return nullptr;
}

CShapes* CEventManager::GetShape(ShapesType iType)
{
	for (auto it : mShapes)
	{
		vtkActor* shapeActor = it->GetActor();
		ShapesType shapeType = GetShapeType(shapeActor);
		if (iType == shapeType)
		{
			return it;
		}
	}
	return nullptr;
}

ShapesType CEventManager::GetShapeType(vtkActor* aShapeActor)
{
	vtkIntArray* retrievedArray = vtkIntArray::SafeDownCast(aShapeActor->GetMapper()->GetInput()->GetFieldData()->GetAbstractArray("ShapesType"));
	ShapesType aShapeType = (ShapesType)retrievedArray->GetValue(0);
	return aShapeType;
}