#include "../header/InteractorStyle.h"

void CInteractorStyle::OnKeyPress()
{
	vtkSmartPointer<vtkCamera> pCamera = this->GetDefaultRenderer()->GetActiveCamera();
	double viewUp[3];
	pCamera->GetViewUp(viewUp);
	vtkRenderWindowInteractor *rwi = this->Interactor;
	std::string key = rwi->GetKeySym();
	double moveDir[3];
	if (key == "Up")
	{
		for (int nCoords = 0; nCoords < 3; nCoords++)
		{
			moveDir[nCoords] = viewUp[nCoords] * 2;
		}
	}
	else if (key == "Down")
	{
		for (int nCoords = 0; nCoords < 3; nCoords++)
		{
			moveDir[nCoords] = viewUp[nCoords] * -2;
		}
	}
	else if (key == "Left")
	{
		double dirProj[3];
		pCamera->GetDirectionOfProjection(dirProj);
		vtkMath::Cross(viewUp, dirProj, moveDir);
	}
	else if (key == "Right")
	{
		double dirProj[3];
		pCamera->GetDirectionOfProjection(dirProj);
		vtkMath::Cross(dirProj, viewUp, moveDir);
	}

	mCylinder.MoveInDirection(moveDir);
	this->GetDefaultRenderer()->GetRenderWindow()->Render();
	CollisionDetection();
	// Forward events
	vtkInteractorStyleTrackballCamera::OnKeyPress();
}