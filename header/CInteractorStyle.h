#include <vtkInteractorStyleTrackballCamera.h>
#include <vtkCamera.h>
#include <vtkMath.h>
#include <vtkBoundingBox.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>

#include "Cylinder.h"
#include "EventManager.h"

class CInteractorStyle : public vtkInteractorStyleTrackballCamera
{
public:
	static CInteractorStyle* New();
	vtkTypeMacro(CInteractorStyle, vtkInteractorStyleTrackballCamera);

	virtual void OnKeyPress()
	{
		vtkSmartPointer<vtkCamera> pCamera = this->GetDefaultRenderer()->GetActiveCamera();
		double viewUp[3];
		pCamera->GetViewUp(viewUp);
		vtkRenderWindowInteractor *rwi = this->Interactor;
		std::string key = rwi->GetKeySym();
		double moveDir[3];
		if (key == "Up")
		{			
			for (int nCoords = 0; nCoords < 3; nCoords++)
			{
				moveDir[nCoords] = viewUp[nCoords] * 2;
			}
		}
		else if (key == "Down")
		{
			for (int nCoords = 0; nCoords < 3; nCoords++)
			{
				moveDir[nCoords] = viewUp[nCoords] * -2;
			}
		}
		else if (key == "Left")
		{
			double dirProj[3];
			pCamera->GetDirectionOfProjection(dirProj);
			vtkMath::Cross(viewUp, dirProj, moveDir);
		}
		else if (key == "Right")
		{
			double dirProj[3];
			pCamera->GetDirectionOfProjection(dirProj);
			vtkMath::Cross(dirProj, viewUp, moveDir);
		}

		mCylinder->MoveInDirection(moveDir);
		this->GetDefaultRenderer()->GetRenderWindow()->Render();
		CollisionDetection();
		// Forward events
		vtkInteractorStyleTrackballCamera::OnKeyPress();
	}

	void SetObject(CCylinder* movableCylinder)
	{
		mCylinder = movableCylinder;
	}
	void SetEventManager(CEventManager& eventManager)
	{
		mEventManager = eventManager;
	}

private:
	CCylinder* mCylinder;
	CEventManager mEventManager;
	

	void CollisionDetection()
	{
		vtkBoundingBox cylinderBB;
		cylinderBB.SetBounds(mCylinder->GetActor()->GetBounds());
		vtkActorCollection* actors = this->GetDefaultRenderer()->GetActors();
		actors->InitTraversal();
		vtkActor* aActor = actors->GetNextActor();
		while (aActor != 0)
		{
			if (aActor != mCylinder->GetActor())
			{
				vtkBoundingBox aBox;
				aBox.SetBounds(aActor->GetBounds());
				if (cylinderBB.Intersects(aBox))
				{
					mEventManager.Collided(aActor, this->GetDefaultRenderer());
					this->GetDefaultRenderer()->GetRenderWindow()->Render();
				}
			}
			aActor = actors->GetNextActor();
		}
	}
};

vtkStandardNewMacro(CInteractorStyle);