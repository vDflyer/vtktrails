#pragma once
#include <vector>
#include <vtkRenderer.h>

#include "Shapes.h"
#include "Cylinder.h"

class CEventManager
{
public:
	CEventManager();
	CEventManager(std::vector<CShapes*> shapes, CCylinder* handler);
	~CEventManager();

	void Collided(vtkActor* aShapeActor, vtkRenderer* renderer);
private:
	ShapesType GetShapeType(vtkActor* aShapeActor);
	CShapes* GetShape(vtkActor* aShapeActor);
	CShapes* GetShape(ShapesType iType);
	void Sphere1Rection(vtkActor* aShapeActor, vtkRenderer* renderer);
	void Sphere2Rection();
	void CubeRection();

	std::vector<CShapes*> mShapes;
	CCylinder* mHandler;
};

