#pragma once
#include <vtkPolyData.h>
#include <vtkActor.h>
#include <vtkSmartPointer.h>

#include "ShapeTypes.h"

class CShapes
{
public:
	CShapes();
	~CShapes();

	vtkSmartPointer<vtkPolyData> GetPolyData();
	vtkSmartPointer<vtkActor> GetActor();
	virtual void React() = 0;
	void Reloacte();
protected:
	virtual void InitShape();
	virtual void ApplyColor();
	void SetType(ShapesType iTypeName);

	vtkSmartPointer<vtkPolyData> mPolyData;
	vtkSmartPointer<vtkActor> mActor;
	double mColor[3];
};

