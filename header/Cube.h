#pragma once
#include "Shapes.h"

class CCube : public CShapes
{
public:
	CCube();
	~CCube();
private:
	virtual void InitShape();
	virtual void ApplyColor();
	virtual void React(){}
};

