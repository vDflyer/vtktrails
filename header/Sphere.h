#pragma once
#include "Shapes.h"

class CSphere : public CShapes
{
public:
	CSphere(ShapesType type);
	~CSphere();

	void DoHide();
private:
	virtual void InitShape();
	virtual void ApplyColor();
	virtual void React(){};


	ShapesType mType;
};

