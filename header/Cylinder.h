#pragma once
#include <vtkCylinderSource.h>

#include "Shapes.h"

class CCylinder : public CShapes
{
public:
	CCylinder();
	//CCylinder(CCylinder&);
	~CCylinder();

	void MoveInDirection(double* dir);
	virtual void React(){}
	void IncreaseHeight();
	void BringToCenter();
private:
	virtual void InitShape();
	virtual void ApplyColor();
	static double mHeight;
};

